﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace ArrivalTimeApp
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "ApiGetArrivalData",
                routeTemplate: "api/{controller}/{id}/arrivals",
                defaults: new { controller = "Stops", action = "Get" }
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional}
            );
        }
    }
}
