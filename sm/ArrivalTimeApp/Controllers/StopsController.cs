﻿using SM.Common.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using SM.Business.ServiceContracts;
using SM.Busness.Services;

namespace ArrivalTimeApp.Controllers
{
    public class StopsController : ApiController
    {
        protected IArrivalService ArrivalService { get; set; }

        public StopsController()
        {
            // Can be replaced with DI
            ArrivalService = new ArrivalService();
        }

        // GET: api/stops/1
        // GET: api/stops/1/arrivals
        public IEnumerable<ArrivalData> Get(int id)
        {
            var arrivalData = ArrivalService.GetArrivalData(id);

            return  arrivalData;
        }
    }
}
