﻿using SM.Common.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM.Business.ServiceContracts
{
    /// <summary>
    /// Service contract for working with arrival information
    /// </summary>
    public interface IArrivalService
    {
        /// <summary>
        /// Returns the arrival information for a given StopId
        /// </summary>
        /// <param name="stopId"></param>
        /// <returns></returns>
        IEnumerable<ArrivalData> GetArrivalData(int stopId);
    }
}
