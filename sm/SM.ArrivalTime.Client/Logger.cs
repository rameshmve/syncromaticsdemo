﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM.ArrivalTime.Client
{
    /// <summary>
    /// Logger class for logging exceptions
    /// </summary>
    public class Logger
    {
        public Logger()
        {

        }

        /// <summary>
        /// Logs the execption. For now, it outputs the message ontothe console.
        /// </summary>
        /// <param name="message"></param>
        public void Log(string message)
        {
            Console.WriteLine(message);
        }
    }
}
