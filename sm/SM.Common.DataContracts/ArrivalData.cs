﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM.Common.DataContracts
{
    /// <summary>
    /// Arrival Data
    /// </summary>
    public class ArrivalData
    {
        /// <summary>
        /// RouteId
        /// </summary>
        public int RouteId { get; set; }

        /// <summary>
        /// List of arrival times
        /// </summary>
        public IEnumerable<int> ArrivalTimes { get; set; }
    }
}
