This application consists of the following 2 components.

SM.ArrivalTime.App
-------------------
- An RESTful Service that has the following endpoints
--	/api/stops/{stopId}
--	/api/stops/{stopId}/arrivals

Sample Url will look like
---------------------------
http://localhost:54822/api/stops/1/arrivals

SM.ArrivalTime.Client
---------------------
- Client application that consumes the RESTful service and
--	Displays next two arrival times for Routes 1,2 and 3 for Stops 1 and 2

The following assumptions/dependencies are being made.
-------------------------------------------------------

1. Within the solution, SM.ArrivalTime.Client project is set as the StartUp project.
2. By default, the RESTFul API runs locallacy on port 54822. 
3. The base Url for the RESTful service is set in the App.Config file.
4. Exceptions are being logged by displaying the exception message onto the console.
5. The application supports 10 Stops with each having 3 routes.
6. Each stop is identified by stopId and each route is identified by routeId.

Technology Stack
-----------------
Visual Studio 2015 / .NET 4.6.1
ASP.NET MVC
ASP.NET Web API
Newtonsoft.Json
