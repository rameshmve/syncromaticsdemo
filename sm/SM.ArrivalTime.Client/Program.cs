﻿using Newtonsoft.Json;
using SM.Common.DataContracts;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Timers;

namespace SM.ArrivalTime.Client
{
    class Program
    {
        // List of StopIds for which the arrival times are requested for
        static List<int> stopIds = new List<int>() { 1, 2 };

        // 1 Minute
        static int callBackIntervalInMilliSeconds = 60000;

        static Timer timer = new Timer(callBackIntervalInMilliSeconds);

        static bool ServiceError = false;

        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Press any key to stop...");
                Console.WriteLine();

                // Run the application to get the arrival times
                Run(); 
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            
            Console.ReadLine();

        }

        private static void Run()
        {

            // Get Arrival Times for the first time
            DisplayArrivalTimes(stopIds);

            //System.Timers.Timer timer = new System.Timers.Timer(callBackIntervalInMilliSeconds);
            timer.Elapsed += OnTimerElapsed;

            timer.Interval = callBackIntervalInMilliSeconds;
            timer.Enabled = true;
            timer.Start();
        }

        private static void OnTimerElapsed(object sender, System.Timers.ElapsedEventArgs e)
        {                
            if (!ServiceError)
            {
                // For each stops, display the next two arrival times, for each of the three routes
                Task.Run(() => DisplayArrivalTimes(stopIds));
            }
            else
            {
                // Stop the timmer
                timer.Stop();              
            }
            
            Console.WriteLine();
        }

        /// <summary>
        /// Displays the arrival times for the given Stop Ids
        /// </summary>//
        /// <param name="stopIds"></param>
        private static async void DisplayArrivalTimes(List<int> stopIds)
        {
            var result = await GetArrivalData(stopIds);

            foreach (var item in result)
            {
                Console.WriteLine(@"Stop {0}:", item.Key);

                if (item.Value != null)
                {
                    foreach (var data in item.Value)
                    {
                        DisplayRouteDetails(data);
                    }
                }
                Console.WriteLine();
            }         
        }

        /// <summary>
        /// Displays the arrival times for each of the three routes
        /// </summary>
        /// <param name="arrivalData"></param>
        private static void DisplayRouteDetails(ArrivalData arrivalData)
        {
            if(arrivalData != null)
            {
                var arrivalTimes = arrivalData.ArrivalTimes.ToList();
                Console.WriteLine(@"Route {0} in {1} mins and {2} mins", arrivalData.RouteId, arrivalTimes[0], arrivalTimes[1]);
            }
        }

        /// <summary>
        /// Gets the arrival data by making a call to the RESTFul API
        /// </summary>
        /// <param name="stopIds"></param>
        /// <returns></returns>
        private static async Task<Dictionary<int, IEnumerable<ArrivalData>>> GetArrivalData(List<int> stopIds)
        {
            Dictionary<int, IEnumerable<ArrivalData>> result = new Dictionary<int, IEnumerable<ArrivalData>>();
            Logger logger = new Logger();

            foreach (var item in stopIds)
            {
                List<ArrivalData> arrivalData = null;

                string serviceUrl = ConfigurationManager.AppSettings["ServiceUrl"].ToString();

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(serviceUrl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    try
                    {
                        // Make async call to the API. Any excpetions will be logged as needed and rethrown.
                        HttpResponseMessage response = await client.GetAsync(string.Format(@"api/Stops/{0}/arrivals", item));
                        response.EnsureSuccessStatusCode();

                        var data = response.Content.ReadAsStringAsync().Result;

                        arrivalData = JsonConvert.DeserializeObject<List<ArrivalData>>(data);
                    }
                    catch (Exception ex)
                    {
                        // Log the exception message
                        logger.Log(ex.Message);
                        Console.WriteLine("Press any key to stop...");

                        ServiceError = true;

                        break;
                    }                                          
                }

                result.Add(item, arrivalData);
            }        
            return result;
        }
    }
}
