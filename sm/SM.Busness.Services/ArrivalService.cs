﻿using SM.Business.ServiceContracts;
using SM.Common.DataContracts;
using System;
using System.Collections.Generic;

namespace SM.Busness.Services
{
    /// <summary>
    /// Service for working with arrival information
    /// </summary>
    public class ArrivalService : IArrivalService
    {
        protected DateTime currentDateTime;
        protected int serviceTimeInMinutes = 15;

        public ArrivalService()
        {
            currentDateTime = DateTime.Now;
       }

        /// <summary>
        /// Returns the arrival information for a given StopId
        /// </summary>
        /// <param name="stopId"></param>
        /// <returns></returns>
        public IEnumerable<ArrivalData> GetArrivalData(int stopId)
        {
            List<ArrivalData> arrivalData = new List<ArrivalData>();

            // For each Routes, get arrival times
            foreach (var item in new[] { 1, 2, 3 })
            {
                arrivalData.Add(new ArrivalData() { RouteId = item, ArrivalTimes = GetArrivalTimes(stopId, item) });
            }

            return arrivalData;
        }

        /// <summary>
        /// Returns next two arrival times base don stopId and routeId
        /// </summary>
        /// <param name="stopId"></param>
        /// <param name="routeId"></param>
        /// <returns></returns>
        private IEnumerable<int> GetArrivalTimes(int stopId, int routeId)
        {
            List<int> arrivalTimes = new List<int>();
            DateTime nextStartDateTime = GetNextStartTimeByRouteId(stopId, routeId);

            int firstArrivalTime = (nextStartDateTime - currentDateTime).Minutes;
            int differenceInStopTime = GetIntervalByStopId(stopId);

            firstArrivalTime += differenceInStopTime;
            firstArrivalTime = (firstArrivalTime == 0) ?  serviceTimeInMinutes : firstArrivalTime;

            firstArrivalTime = (firstArrivalTime > serviceTimeInMinutes) ? (firstArrivalTime - serviceTimeInMinutes) : firstArrivalTime;

            arrivalTimes.Add(firstArrivalTime);
            arrivalTimes.Add(firstArrivalTime + serviceTimeInMinutes);

            return arrivalTimes;
        }

        /// <summary>
        /// Returns time interval in minutes between stops
        /// </summary>
        /// <param name="stopId"></param>
        /// <returns></returns>
        private int GetIntervalByStopId(int stopId)
        {
            int differenceInStopTime = 0;

            switch (stopId)
            {
                case 1:
                    differenceInStopTime = 0;
                    break;
                case 2:
                    differenceInStopTime = 2;
                    break;
                case 3:
                    differenceInStopTime = 4;
                    break;
                case 4:
                    differenceInStopTime = 6;
                    break;
                case 5:
                    differenceInStopTime = 8;
                    break;
                case 6:
                    differenceInStopTime = 10;
                    break;
                case 7:
                    differenceInStopTime = 12;
                    break;
                case 8:
                    differenceInStopTime = 14;
                    break;
                case 9:
                    differenceInStopTime = 16;
                    break;
                case 10:
                    differenceInStopTime = 18;
                    break;
                default:
                    differenceInStopTime = 0;
                    break;
            }

            return differenceInStopTime;
        }

        /// <summary>
        /// Returns the next start time based on stopId and routeId
        /// </summary>
        /// <param name="stopId"></param>
        /// <param name="routeId"></param>
        /// <returns></returns>
        private DateTime GetNextStartTimeByRouteId(int stopId, int routeId)
        {
            DateTime dtNextStartDateTime = currentDateTime;

            int currentMinute = dtNextStartDateTime.Minute == 0 ? 60 : dtNextStartDateTime.Minute;
            int differenceInMinutes= 0;
            int minutesToAdd = 0;
            
            differenceInMinutes = (currentMinute % serviceTimeInMinutes == 0) ? 0 : (serviceTimeInMinutes - currentMinute % serviceTimeInMinutes);
            int runningMinuteDifference = GetRunningMinuteDifferenceByRouteId(routeId);

            minutesToAdd = (differenceInMinutes + runningMinuteDifference);

            return dtNextStartDateTime.AddMinutes(minutesToAdd);
        }

        /// <summary>
        /// Returns difference in running time between routes in minutes
        /// </summary>
        /// <param name="routeId"></param>
        /// <returns>runningMinuteDifference</returns>
        private int GetRunningMinuteDifferenceByRouteId(int routeId)
        {
            int runningMinuteDifference = 0;

            switch (routeId)
            {
                case 1:
                    runningMinuteDifference = 0;
                    break;
                case 2:
                    runningMinuteDifference = 2;
                    break;
                case 3:
                    runningMinuteDifference = 4;
                    break;
                default:
                    runningMinuteDifference = 0;
                    break;
            }

            return runningMinuteDifference;
        }
    }
}
